
**** DEPRECATED...please see https://github.com/stantonious/ss-apps.git ****

To Setup

```
sudo apt install -y git
cd /tmp
git clone https://bitbucket.org/stantonious/ss-apps.git
ss-apps/setup/scripts/ss-setup.sh
```

To Install apps

`pip install git+https://git@bitbucket.org/stantonious/ss-apps.git#subdirectory="pi-core"`

`pip install git+https://git@bitbucket.org/stantonious/ss-apps.git#subdirectory="pi-apps/br"`

`pip install git+https://git@bitbucket.org/stantonious/ss-apps.git#subdirectory="pi-apps/inf"`

`pip install git+https://git@bitbucket.org/stantonious/ss-apps.git#subdirectory="pi-web/inf_gui"`

`pip install git+https://git@bitbucket.org/stantonious/ss-apps.git#subdirectory="pi-svc/audio_playback"`
